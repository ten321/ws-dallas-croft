# Dallas Croft
## v0.1

This is the WordPress theme for [Dallas Croft](http://www.dallascroft.com/)

## Base Framework

This theme is based off of the Genesis Framework by StudioPress.

## Changelog

### 0.1

* Initial version

## Contributors

* Designer: [White Spider Design](http://whitespiderdesign.com)
* Developer: [Ten-321 Enterprises](http://ten-321.com/)