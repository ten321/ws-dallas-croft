<?php
/**
 * Primary functionality for the Dallas Croft 2017 WordPress theme
 *
 * @package    WordPress
 * @subpackage Dallas Croft
 * @version    0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'Dallas_Croft' ) ) {
	class Dallas_Croft {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.1.10';
		
		/**
		 * Holds the class instance.
		 *
		 * @since   0.1
		 * @access	private
		 * @var		Dallas_Croft
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return	Dallas_Croft
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the object and set up the appropriate actions
		 *
		 * @access  private
		 * @since   0.1
		 */
		private function __construct() {
			//* Child theme (do not remove)
			define( 'CHILD_THEME_NAME', 'Dallas Croft 2017' );
			define( 'CHILD_THEME_URL', 'http://www.dallascroft.com/' );
			define( 'CHILD_THEME_VERSION', $this->version );
			
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			add_action( 'init', array( $this, 'init' ) );
		}
		
		/**
		 * Register/enqueue any required style sheets and script files
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function add_scripts_and_styles() {
			wp_register_script( 'flexslider', get_stylesheet_directory_uri() . '/scripts/jquery-plugins/flexslider/jquery.flexslider-min.js', array( 'jquery' ), $this->version, true );
			wp_register_style( 'flexslider-css', get_stylesheet_directory_uri() . '/scripts/jquery-plugins/flexslider/flexslider.css', array(), $this->version, 'all' );
			if ( is_front_page() ) {
				wp_enqueue_script( 'flexslider' );
				wp_enqueue_style( 'flexslider-css' );
			}
			wp_register_style( 'genesis', get_stylesheet_uri(), array( 'dashicons' ), $this->version, 'all' );
			wp_enqueue_style( 'dallas-croft', get_stylesheet_directory_uri() . '/dallas-croft.css', array( 'genesis', 'dashicons' ), $this->version, 'all' );
		}
		
		/**
		 * Perform any actions that need to occur before the page is output
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function template_redirect() {
			if ( is_front_page() ) {
				remove_all_actions( 'genesis_loop' );
				remove_all_actions( 'genesis_before_loop' );
				remove_all_actions( 'genesis_after_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page' ) );
			} else if ( is_page() ) {
				add_action( 'genesis_after_header', array( $this, 'insert_featured_image' ) );
			} else if ( is_post_type_archive( 'testimonial' ) ) {
				remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );
			}
		}
		
		/**
		 * Perform any necessary modifications to the Genesis defaults
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function adjust_genesis() {
			add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-accessibility', array(
				'404-page',
				'drop-down-menu',
				'headings',
				'rems',
				'search-form',
				'skip-links'
			) );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'menu-primary',
				'menu-secondary',
				/*'site-inner',*/
				'footer-widgets',
				'footer',
				'front-sidebar',
			) );
			
			/** Add support for a custom logo */
			add_theme_support( 'custom-logo', array(
				'default-image' => get_stylesheet_directory_uri() . '/images/logo.png',
				'width'       => 215,
				'height'      => 113,
				'flex-height' => true,
			) );
			
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			remove_all_actions( 'genesis_header' );
			add_action( 'genesis_header', array( $this, 'do_header' ) );
			remove_all_actions( 'genesis_before_footer' );
			remove_action( 'genesis_footer', 'genesis_do_footer' );
			add_action( 'genesis_footer', array( $this, 'do_footer' ) );
			add_action( 'genesis_after_footer', array( $this, 'do_sub_footer' ) );
			
			remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
			add_filter( 'genesis_post_info', array( $this, 'genesis_post_info' ) );
			
			global $content_width;
			$content_width = 1280;
		}
		
		/**
		 * Register any necessary widgetized areas
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function register_sidebars() {
			add_theme_support( 'genesis-footer-widgets', 4 );
			/**
			 * Front page widget areas
			 */
			genesis_register_sidebar( array(
				'name'        => __( '[Front Page] Feature Listings', 'dallas-croft' ),
				'description' => __( 'Displays just below the main hero image on the front page, and is intended to be used to feature home listings', 'dallas-croft' ),
				'id'          => 'front-feature-listings',
			) );
			genesis_register_sidebar( array(
				'name'        => __( '[Front Page] Secondary Features', 'dallas-croft' ),
				'description' => __( 'Displays below the featured listings, and gives the opportunity to display at least two different widget blocks for secondary website features', 'dallas-croft' ),
				'id'          => 'front-secondary',
			) );
			genesis_register_sidebar( array(
				'name'        => __( '[Front Page] Above Footer', 'dallas-croft' ),
				'description' => __( 'Displays just above the site footer on the front page', 'dallas-croft' ),
				'id'          => 'front-footer',
			) );
			
			unregister_sidebar( 'sidebar-alt' );
		}
		
		/**
		 * Perform any actions that need to occur at the start of the bootstrap
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function init() {
			add_image_size( 'home-feature', 1600, 800, true );
			add_image_size( 'hero-image', 1600, 560, true );
			
			add_shortcode( 'number-format', array( $this, 'number_format' ) );
			add_shortcode( 'currency', array( $this, 'money_format' ) );
		}
		
		/*
		 * Output the page header
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_header() {
			echo '<header class="site-header">';
			genesis_structural_wrap( 'header' );
			if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			}
			genesis_structural_wrap( 'header', 'close' );
			echo '</header>';
		}
		
		/**
		 * Output the page footer
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_footer() {
			foreach ( range( 1, 4 ) as $i ) {
				if ( is_active_sidebar( 'footer-' . $i ) ) {
					printf( '<section class="footer-widgets footer-widget-area-%d">', $i );
					genesis_structural_wrap( 'footer-widgets' );
					dynamic_sidebar( 'footer-' . $i );
					genesis_structural_wrap( 'footer-widgets', 'close' );
					echo '</section>';
				}
			}
		}
		
		/**
		 * Output the White Spider and copyright notice
		 */
		function do_sub_footer() {
			echo '<div class="sub-footer">';
			genesis_structural_wrap( 'footer' );
			$this->white_spider_notice();
			genesis_structural_wrap( 'footer', 'close' );
			echo '</div>';
		}
		
		/**
		 * Insert the featured image at the top of a single page
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function insert_featured_image() {
			if ( ! is_page() ) {
				return;
			}
			
			if ( ! has_post_thumbnail() ) {
				return;
			}
			
			printf( '<figure class="hero-image">%s</figure>', get_the_post_thumbnail( get_the_ID(), 'hero-image' ) );
		}
		
		/**
		 * Output the main content of the front page
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		public function do_front_page() {
			$front = get_option( 'page_on_front', false );
			if ( has_post_thumbnail( $front ) ) {
				printf( '<figure class="hero-image front-hero">%s</figure>', get_the_post_thumbnail( $front, 'home-feature' ) );
			} else {
				printf( "\n<!-- The page with an ID of %d does not seem to have a featured image -->\n", $front );
			}
			$sidebars = array( 'front-feature-listings', 'front-secondary', 'front-footer' );
			foreach ( $sidebars as $s ) {
				if ( is_active_sidebar( $s ) ) {
					printf( '<section class="%s">', $s );
					genesis_structural_wrap( 'front-sidebar' );
					dynamic_sidebar( $s );
					genesis_structural_wrap( 'front-sidebar', 'close' );
					echo '</section>';
				}
			}
		}
		
		/**
		 * Modify the entry header for blog posts
		 *
		 * @param   $info string the existing shortcode string being modified
		 *
		 * @access  public
		 * @since   0.1
		 * @return  string the updated shortcode string
		 */
		public function genesis_post_info( $info ) {
			return '[post_date] [post_comments] [post_edit]';
		}
		
		/**
		 * Format and return a number in a readable format
		 *
		 * @param $atts array the list of shortcode attributes
		 * @param $content string the number that needs to be formatted
		 *
		 * @access public
		 * @since  0.2
		 * @return string the formatted number
		 */
		public function number_format( $atts=array(), $content=null ) {
			return number_format( floatval( wpv_do_shortcode( $content ) ) );
		}
		
		/**
		 * Format and return a number as currency
		 *
		 * @param $atts array the list of shortcode attributes
		 * @param $content string the number that needs to be formatted
		 *
		 * @access public
		 * @since  0.2
		 * @return string the formatted number
		 */
		public function money_format( $atts=array(), $content=null ) {
			setlocale( LC_MONETARY, 'en_US.UTF-8' );
			return money_format( '%i', floatval( wpv_do_shortcode( $content ) ) );
		}
		
		/**
		 * Output the White Spider notice
		 *
		 * @access  private
		 * @since   0.1
		 * @return  void
		 */
		private function white_spider_notice() {
			$notice = array();
			$notice[] = sprintf( '&copy;%1$s %2$s All Rights Reserved', date( 'Y' ), get_bloginfo( 'name' ) );
			$notice[] = 'Prices and Information Listed on This Website Are Subject to Change';
			$notice[] = 'Equal Housing Opportunity';
			
			$notice = implode( ' | ', $notice );
			
			printf( '<div id="white-spider-copyright-widget-1" class="widget white-spider-copyright"><div class="widget-wrap"><div class="textwidget">%s</div></div></div>', $notice );
		}
	}
}

global $croft_object;
$croft_object = Dallas_Croft::instance();